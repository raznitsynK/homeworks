import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class homework1 {
    public static void main(String[] args) {
        Random random = new Random();
        int number = random.nextInt(100);
        System.out.println("Let the game begin!");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input your name:");
        String name = scanner.nextLine();
        boolean continueGuess = true;
        int[] inputs = new int[30];
        int i = 0;
        for (; continueGuess == true; i++) {
            System.out.println("Please Guess the number: ");
            int guess = scanner.nextInt();
            inputs[i] = guess;
            if (guess == number) {
                System.out.println(name + ", congratulations you did it!");
                continueGuess = false;
            } else if (guess < number) {
                System.out.println("Your number is too small. Please, try again: ");
            } else {
                System.out.println("Your number is too big. Please, try again: ");
            }

        }
        int[] inputs1 = new int[i];
        System.out.println("Number of your attempts: " + i);
        System.out.println("All your Inputs: ");
        for (int a = 0; a < i; a++) {
            inputs1[a] = inputs[a];
            System.out.print(inputs1[a] + ", ");
        }
        System.out.println('\n' + "All your Inputs, after sorting: ");
        Arrays.sort(inputs1);
        for (int a = 0; a < inputs1.length; a++) {
            System.out.print(inputs1[a] + ", ");
        }
    }
}
